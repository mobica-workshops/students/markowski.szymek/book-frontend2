import axios from 'axios'
import { useConfigStore } from "@/store/ConfigStore";

const apiClient = axios.create({
  withCredentials: false,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  }
})

export default {
  async getBooks() {
    const configStore = useConfigStore()
    await configStore.loadConfiguration()
    return apiClient.get(configStore.configuration.apiUrl + '/book-list/v1/books')
  }
}
