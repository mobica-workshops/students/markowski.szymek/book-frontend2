openapi: 3.0.0
servers:
  - description: Development API endpoint
    url: 'http://localhost:8881'
  - description: Docker API endpoint
    url: 'http://localhost:8881'
  - description: Private API endpoint
    url: 'http://book-bff'

info:
  description: This is a simple API
  version: "1.0.0"
  title: book-bff
  contact:
    email: atulodzi@gmail.com
  license:
    name: Apache 2.0
    url: 'https://www.apache.org/licenses/LICENSE-2.0.html'

tags:
  - name: health
    description: Health endpoints
  - name: book-gateway
    description: BookGateway related endpoints
  - name: book-list
    description: BookList related endpoints

paths:

  ###############
  # health path #
  ###############

  '/v1/health':
    get:
      tags:
        - health
      summary: Get Health
      operationId: getHealth
      description: |
        This endpoint is sending health information
      responses:
        '200':
          $ref: '#/components/responses/HealthResponse'
        '500':
          $ref: '#/components/responses/InternalServerErrorResponse'
        '501':
          $ref: '#/components/responses/NotImplementedResponse'

  ######################
  # book-gateway paths #
  ######################

  '/v1/login':
    post:
      tags:
        - book-gateway
      summary: Login
      operationId: loginUser
      description: |
        This endpoint is login user into the service and returning JWT Token
      requestBody:
        $ref: '#/components/requestBodies/LoginUserRequest'
      responses:
        '201':
          $ref: '#/components/responses/LoginUserResponse'
        default:
          $ref: '#/components/responses/DefaultErrorResponse'

  '/v1/protected':
    get:
      summary: 'Sample protected endpoint: Returns message when token is correct'
      operationId: protectedEndpoint
      tags:
        - protected
      security:
        - jwtAuth: []
      responses:
        '200':
          $ref: '#/components/responses/SuccessResponse'
        '401':
          $ref: '#/components/responses/UnauthorizedErrorResponse'
        default:
          $ref: '#/components/responses/DefaultErrorResponse'

  '/book-list/v1/health':
    get:
      tags:
        - protected
      security:
        - jwtAuth: []
      summary: Get Book List Health
      operationId: getBookListHealth
      description: |
        This endpoint is sending health information
      responses:
        '200':
          $ref: '#/components/responses/BookListHealthResponse'
        '500':
          $ref: '#/components/responses/InternalServerErrorResponse'
        '501':
          $ref: '#/components/responses/NotImplementedResponse'

  '/book-admin/v1/health':
    get:
      tags:
        - protected
      security:
        - jwtAuth: []
      summary: Get Book Admin Health
      operationId: getBookAdminHealth
      description: |
        This endpoint is sending health information
      responses:
        '200':
          $ref: '#/components/responses/BookAdminHealthResponse'
        '500':
          $ref: '#/components/responses/InternalServerErrorResponse'
        '501':
          $ref: '#/components/responses/NotImplementedResponse'

  '/book-admin/v1/admins':
    post:
      tags:
        - protected
      summary: Create BookAdmin
      operationId: createBookAdmin
      description: |
        This endpoint creating new book-admin and is sending details of book-admin with assigned ID\n
      requestBody:
        $ref: '#/components/requestBodies/CreateBookAdminRequest'
      responses:
        '201':
          $ref: '#/components/responses/CreateBookAdminResponse'
        default:
          $ref: '#/components/responses/DefaultErrorResponse'
    get:
      tags:
        - protected
      summary: Get BookAdmins
      operationId: getBookAdmins
      description: |
        This endpoint is sending details of all book-admins\n
      parameters:
        - in: query
          name: skip
          description: how many records to skip
          required: false
          schema:
            type: integer
            format: int64
            minimum: 0
            default: 0
        - in: query
          name: limit
          description: how many records to return
          required: false
          schema:
            type: integer
            format: int64
            minimum: 1
            default: 10
        - in: query
          name: email
          description: filer with email
          required: false
          schema:
            type: string
            default: ""
      responses:
        '200':
          $ref: '#/components/responses/BookAdminsResponse'
        default:
          $ref: '#/components/responses/DefaultErrorResponse'

  '/book-admin/v1/admins/{id}':
    get:
      tags:
        - protected
      summary: Get BookAdmin by ID
      operationId: getBookAdminById
      description: |
        This endpoint is sending details of book-admin chosen by ID\n
      parameters:
        - in: path
          name: id
          description: The ID of an admin
          required: true
          schema:
            type: string
      responses:
        '200':
          $ref: '#/components/responses/BookAdminResponse'
        '404':
          $ref: '#/components/responses/NotFoundErrorResponse'
        default:
          $ref: '#/components/responses/DefaultErrorResponse'
    put:
      tags:
        - protected
      summary: Put BookAdmin by ID
      operationId: putBookAdminById
      description: This endpoint is updating book-admin\n
      parameters:
        - in: path
          name: id
          description: The ID of an book-admin
          required: true
          schema:
            type: string
      requestBody:
        $ref: '#/components/requestBodies/UpdateBookAdminRequest'
      responses:
        '202':
          $ref: '#/components/responses/BookAdminResponse'
        '404':
          $ref: '#/components/responses/NotFoundErrorResponse'
        default:
          $ref: '#/components/responses/DefaultErrorResponse'
    delete:
      tags:
        - protected
      summary: Delete BookAdmin by ID
      operationId: deleteBookAdminById
      description: This endpoint is deleting book-admin\n
      parameters:
        - in: path
          name: id
          description: The ID of an admin
          required: true
          schema:
            type: string
      responses:
        '202':
          $ref: '#/components/responses/SuccessResponse'
        '404':
          $ref: '#/components/responses/NotFoundErrorResponse'
        default:
          $ref: '#/components/responses/DefaultErrorResponse'

  '/book-list/v1/books':
    post:
      tags:
        - protected
      summary: Create Book
      operationId: createBook
      description: |
        This endpoint creating new book-list and is sending details of book-list with assigned ID
      requestBody:
        $ref: '#/components/requestBodies/CreateBookRequest'
      responses:
        '201':
          $ref: '#/components/responses/CreateBookResponse'
        default:
          $ref: '#/components/responses/DefaultErrorResponse'
    get:
      tags:
        - book-list
      summary: Get Books
      operationId: getBooks
      description: |
        This endpoint is sending details of all book-lists
      parameters:
        - in: query
          name: skip
          description: how many records to skip
          required: false
          schema:
            type: integer
            format: int64
            minimum: 0
            default: 0
        - in: query
          name: limit
          description: how many records to return
          required: false
          schema:
            type: integer
            format: int64
            minimum: 1
            default: 10
      responses:
        '200':
          $ref: '#/components/responses/BooksResponse'
        default:
          $ref: '#/components/responses/DefaultErrorResponse'

  '/book-list/v1/books/{id}':
    get:
      tags:
        - book-list
      summary: Get Book by ID
      operationId: getBookById
      description: |
        This endpoint is sending details of book-list chosen by ID
      parameters:
        - in: path
          name: id
          description: 'The ID of a book'
          required: true
          schema:
            type: integer
      responses:
        '200':
          $ref: '#/components/responses/BookResponse'
        '404':
          $ref: '#/components/responses/NotFoundErrorResponse'
        default:
          $ref: '#/components/responses/DefaultErrorResponse'
    put:
      tags:
        - protected
      summary: Put Book by ID
      operationId: putBookById
      description: |
        This endpoint is updating book-list
      parameters:
        - in: path
          name: id
          description: 'The ID of a book'
          required: true
          schema:
            type: integer
      requestBody:
        $ref: '#/components/requestBodies/UpdateBookRequest'
      responses:
        '202':
          $ref: '#/components/responses/BookResponse'
        '404':
          $ref: '#/components/responses/NotFoundErrorResponse'
        default:
          $ref: '#/components/responses/DefaultErrorResponse'
    delete:
      tags:
        - protected
      summary: Delete Book by ID
      operationId: deleteBookById
      description: |
        This endpoint is deleting book-list
      parameters:
        - in: path
          name: id
          description: 'The ID of a book'
          required: true
          schema:
            type: integer
      responses:
        '202':
          $ref: '#/components/responses/SuccessResponse'
        '404':
          $ref: '#/components/responses/NotFoundErrorResponse'
        default:
          $ref: '#/components/responses/DefaultErrorResponse'

components:

  requestBodies:

    LoginUserRequest:
      description: Login user
      required: true
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/LoginUserInput'
    CreateBookAdminRequest:
      description: Creating book-admin
      required: true
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/CreateBookAdminInput'
    UpdateBookAdminRequest:
      description: Updating book-admin
      required: true
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/UpdateBookAdminInput'
    CreateBookRequest:
      description: Creating book-list
      required: true
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/CreateBookInput'
    UpdateBookRequest:
      description: Updating book-list
      required: true
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/UpdateBookInput'

  responses:

    ######################
    # standard responses #
    ######################

    SuccessResponse:
      description: 'Default Success response'
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/Success'

    NotImplementedResponse:
      description: Not Implemented
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/Error'

    InternalServerErrorResponse:
      description: Internal Server Error
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/Error'

    NotFoundErrorResponse:
      description: Not Found
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/Error'

    UnauthorizedErrorResponse:
      description: Access token is missing or invalid
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/Error'

    DefaultErrorResponse:
      description: Unexpected error
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/Error'

    ###################
    # health response #
    ###################

    HealthResponse:
      description: 'Health information'
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/Health'

    ########################
    # book-admin responses #
    ########################

    LoginUserResponse:
      description: 'Success response for the login request'
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/LoginUserOutput'

    BookListHealthResponse:
      description: 'Health information'
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/BookListHealth'

    BookAdminHealthResponse:
      description: 'Health information'
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/BookAdminHealth'
    CreateBookAdminResponse:
      description: 'Success response for the book-admin creation request'
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/CreateBookAdminOutput'
    BookAdminsResponse:
      description: 'Success response for the book-admins request'
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/BookAdminsOutput'
    BookAdminResponse:
      description: 'Success response for the book-admin request'
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/BookAdminOutput'
    CreateBookResponse:
      description: 'Success response for the book-list creation request'
      content:
        application/json:
          schema:
              $ref: '#/components/schemas/CreateBookOutput'
    BooksResponse:
      description: 'Success response for the book-lists request'
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/BooksOutput'
    BookResponse:
      description: 'Success response for the book-list request'
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/BookOutput'
  schemas:

    ####################
    # standard objects #
    ####################

    Success:
      type: object
      properties:
        status:
          type: integer
          default: 200
        message:
          type: string
        content:
          type: object
          nullable: true

    Error:
      type: object
      properties:
        status:
          type: integer
          example: 404
        message:
          type: string
          example: "book-admin not found"
        errors:
          type: array
          items:
            type: string
          nullable: true

    ##################
    # health objects #
    ##################

    Health:
      type: object
      properties:
        status:
          type: integer
          default: 200
        message:
          type: string
          default: "book gateway health"
        content:
          $ref: '#/components/schemas/HealthContent'

    HealthContent:
      type: object
      properties:
        alive:
          type: boolean
        redis:
          type: boolean
        servicesHealth:
          type: object
          properties:
            bookList:
              type: boolean
            bookAdmin:
              type: boolean

    ######################
    # book-admin objects #
    ######################

    LoginUserInput:
      type: object
      required:
        - email
        - password
      properties:
        email:
          type: string
          nullable: false
        password:
          type: string
          nullable: false
        returnSecureToken:
          type: boolean
          nullable: true

    LoginUserOutput:
      type: object
      properties:
        status:
          type: integer
          default: 201
        message:
          type: string
        content:
          $ref: '#/components/schemas/JwtTokenObject'

    JwtTokenObject:
      type: object
      required:
        - token
      properties:
        token:
          type: string

    BookListHealth:
      type: object
      properties:
        status:
          type: integer
          default: 200
        message:
          type: string
          default: "book list api health"
        content:
          $ref: '#/components/schemas/BookListHealthContent'

    BookListHealthContent:
      type: object
      properties:
        alive:
          type: boolean
        postgres:
          type: boolean

    BookAdminHealth:
      type: object
      properties:
        status:
          type: integer
          default: 200
        message:
          type: string
          default: "book admin api health"
        content:
          $ref: '#/components/schemas/BookAdminHealthContent'

    BookAdminHealthContent:
      type: object
      properties:
        alive:
          type: boolean
        mongo:
          type: boolean
    
    ######################
    # book-admin objects #
    ######################
    CreateBookAdminInput:
      type: object
      required:
        - email
        - password
        properties:
          email:
            type: string
            nullable: false
          firstName:
            type: string
            nullable: false
          lastName:
            type: string
            nullable: false
          password:
            type: string
            nullable: false
          version:
            type: integer
            default: 1
            nullable: false

    CreateBookAdminOutput:
      type: object
      properties:
        status:
          type: integer
          default: 201
        message:
          type: string
        content:
          $ref: '#/components/schemas/CreatedBookAdmin'

    CreatedBookAdmin:
      type: object
      properties:
        id:
          type: string
          description: id of the created object

    UpdateBookAdminInput:
      type: object
      required:
        - email
        - password
        properties:
          email:
            type: string
            nullable: false
          firstName:
            type: string
            nullable: false
          lastName:
            type: string
            nullable: false
          password:
            type: string
            nullable: false
          version:
            type: integer
            default: 1
            nullable: false
    BookAdminsOutput:
      type: object
      required:
        - status
        - message
        - content
      properties:
        status:
          type: integer
          default: 200
        message:
          type: string
        content:
          $ref: '#/components/schemas/PaginatedBookAdminsContent'

    PaginatedBookAdminsContent:
      type: object
      required:
        - count
        - skip
        - limit
        - results
      properties:
        count:
          type: integer
          example: 42
        skip:
          type: integer
          example: 0
          default: 0
        limit:
          type: integer
          example: 10
          default: 10
        results:
          type: array
          items:
            $ref: '#/components/schemas/BookAdminObject'

    BookAdminOutput:
      type: object
      required:
        - status
        - message
        - content
      properties:
        status:
          type: integer
          default: 200
        message:
          type: string
        content:
          $ref: '#/components/schemas/BookAdminObject'

    BookAdminObject:
      type: object
      required:
        - id
        - email
        - firstName
        - lastName
        - passwordHash
        - version
      properties:
        id:
          type: string
          description: ID of the BookAdmin in the Database
          readOnly: true
        email:
          type: string
          nullable: false
        firstName:
          type: string
          nullable: false
        lastName:
          type: string
          nullable: false
        passwordHash:
          type": string
          nullable: false
        version:
          type: integer
          default: 1
          nullable: false

    ######################
    # book-list objects #
    ######################

    CreateBookInput:
      $ref: '#/components/schemas/BookObject'

    CreateBookOutput:
      type: object
      properties:
        status:
          type: integer
          default: 201
        message:
          type: string
        content:
          $ref: '#/components/schemas/CreatedBook'

    CreatedBook:
      type: object
      properties:
        id:
          type: integer
          description: "id of the created object"

    UpdateBookInput:
      $ref: '#/components/schemas/BookObject'

    BooksOutput:
      type: object
      required:
        - status
        - message
        - content
      properties:
        status:
          type: integer
          default: 200
        message:
          type: string
        content:
          $ref: '#/components/schemas/PaginatedBooksContent'

    PaginatedBooksContent:
      type: object
      required:
        - count
        - skip
        - limit
        - results
      properties:
        count:
          type: integer
          example: 42
        skip:
          type: integer
          example: 0
          default: 0
        limit:
          type: integer
          example: 10
          default: 10
        results:
          type: array
          items:
            $ref: '#/components/schemas/BookObject'

    BookOutput:
      type: object
      required:
        - status
        - message
        - content
      properties:
        status:
          type: integer
          default: 200
        message:
          type: string
        content:
          $ref: '#/components/schemas/BookObject'

    BookObject:
      type: object
      required:
        - id
        - title
        - author
        - year
      properties:
        id:
          type: integer
          description: ID of the Book in the Database
          readOnly: true
        title:
          type: string
          nullable: false
        author:
          type: string
          nullable: false
        year:
          type: string
          nullable: false
        description:
          type: string
          nullable: true
        image:
          $ref: '#/components/schemas/ImageObject'

    ImageObject:
      type: object
      nullable: true
      properties:
        type:
          type: string
          nullable: false
          enum:
          - base64
          - url
        data:
          type: string
          nullable: false

  securitySchemes:
    jwtAuth:
      type: http
      scheme: bearer
      bearerFormat: JWT    # optional, arbitrary value for documentation purposes

security:
  - jwtAuth: []
